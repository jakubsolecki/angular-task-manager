import { Component, HostListener } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  @HostListener('window:beforeunload', ['$event'])
  beforeLoad($event) {
      $event.returnValue = 'Your data will be lost!';
  }
}
