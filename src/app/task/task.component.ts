import { Component, OnInit, Input, ChangeDetectorRef, ChangeDetectionStrategy, EventEmitter } from '@angular/core';
import { Task } from '../models/task.model';
import * as moment from 'moment';
import { Config } from '../shared/config';
import { TasksService } from '../services/TasksService';

@Component({
  selector: 'task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TaskComponent implements OnInit {

  interval: any;
  timerStatus: boolean = false;
  currentDuration = 0;
  @Input() task: Task;

  constructor(private changeDatectorRef: ChangeDetectorRef, private tasksService: TasksService) { }


  ngOnInit() {
    this.task.duration = 0;
    this.task = new Task(this.task);
  }

  get currentTimerLabel() {
    return this.timerStatus ? 'Pauza' : 'Start';
  }

  handleTimer() {
    if (!this.timerStatus) {
      this.task.start_date = moment().format(Config.DATE_FORMAT);
      this.interval = setInterval(() => {
        this.changeDatectorRef.detectChanges();
      }, 1000);
    } else {
      this.task.duration = this.task.duration + this.task.currentDiff;
      this.task.start_date = null;
      clearInterval(this.interval);
    }
    this.timerStatus = !this.timerStatus;
  }

  handleRemove() {
    this.tasksService.removeTask(this.task);
  }

  handleEdit() {
    this.tasksService.editTask(this.task);
  }

}
