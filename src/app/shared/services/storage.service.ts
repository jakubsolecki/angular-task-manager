import { Injectable } from '@angular/core';

@Injectable()
export class StorageService {
  constructor() {}

  get(key: string) {
    return localStorage.getItem(key);
  }

  put(key: string, val: any) {
    localStorage.setItem(key, val);
  }
}
