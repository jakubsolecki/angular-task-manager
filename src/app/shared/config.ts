export class Config {
  static DATE_FORMAT: string = 'DD/MM/YYYY HH:mm:ss';
  static DURATION_FORMAT: string = 'HH:mm:ss';
}
