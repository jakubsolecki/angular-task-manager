import { Injectable, EventEmitter } from '@angular/core';
import { Task } from '../models/task.model';
import * as moment from 'moment';
import { Config } from '../shared/config'
import { StorageService } from '../shared/services/storage.service';

@Injectable()
export class TasksService {

    private tasks: Task[] = [];
    public eventEmitter:EventEmitter<boolean> = new EventEmitter();
    public editEmitter:EventEmitter<Task> = new EventEmitter();

    constructor(private storageService: StorageService){
        this.tasks = JSON.parse(this.storageService.get('tasks'));

    }

    getTasks() {
        return this.tasks;
    }

    addTask(task:Task) {
        task.created_date = moment().format(Config.DATE_FORMAT);
        
        if(this.tasks){
            this.tasks.unshift(task);
        } else {
            this.tasks = [task];
        }
        this.updateTasksList();
    }

    updateTasksList() {
        this.storageService.put('tasks', JSON.stringify(this.tasks));
        this.eventEmitter.emit(true);
    }

    removeTask(task) {
        this.tasks = this.tasks.filter((item,index) => {
            return item.created_date !== task.created_date;
        });
        this.storageService.put('tasks', JSON.stringify(this.tasks));
        this.eventEmitter.emit(true);
    }

    editTask(task) {
        this.editEmitter.emit(task);
    }
}