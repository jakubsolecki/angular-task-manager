import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { TasksService } from '../services/TasksService';

@Component({
  selector: 'create-task',
  templateUrl: './create-task.component.html',
  styleUrls: ['./create-task.component.scss']
})
export class CreateTaskComponent implements OnInit {
  createTaskForm: FormGroup;
  constructor(private fb: FormBuilder, private tasksService: TasksService) {}
  submitted: boolean = false;
  ngOnInit() {
    this.createForm();

    this.tasksService.editEmitter.subscribe((task) => {
      this.createTaskForm.setValue({
        name: task.name,
        description: task.description
      });
    })
  }
  createForm() {
    this.createTaskForm = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(255)]],
      description: ['', [Validators.minLength(2), Validators.maxLength(255)]],
    });
  }

  get taskName() {
    return this.createTaskForm.get('name');
  }

  get taskDescription() {
    return this.createTaskForm.get('description');
  }

  onSubmit() {
    this.submitted = true;
    console.warn(this.createTaskForm.value, 'form values');
    if (this.createTaskForm.valid) {
      this.tasksService.addTask(this.createTaskForm.value);
      this.createTaskForm.reset();
      this.submitted = false;
    }
  }


}
