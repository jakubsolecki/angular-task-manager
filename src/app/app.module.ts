import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
import { TimerDeclarations } from './shared/declarations/timer.declarations';
import { StorageService } from './shared/services/storage.service';
import { TasksService } from './services/TasksService';
import { CreateTaskComponent } from './create-task/create-task.component';
import { TasksListComponent } from './tasks-list/tasks-list.component';
import { TaskComponent } from './task/task.component';

@NgModule({
  declarations: [AppComponent, ...TimerDeclarations, CreateTaskComponent, TasksListComponent, TaskComponent],
  imports: [BrowserModule, ReactiveFormsModule],
  providers: [StorageService, TasksService],
  bootstrap: [AppComponent]
})
export class AppModule {}
