import * as moment from 'moment';
import { Config } from '../shared/config';

export class Task {
  name: string = null;
  description: string = null;
  created_date: string = null;
  start_date: string = null;
  duration: number = 0;

  constructor(fields?: any) {
    if (fields) {
      Object.assign(this, fields);
    }
  }

  get currentDiff() {
    if (this.start_date)
      return moment().diff(moment(this.start_date, Config.DATE_FORMAT));
    return 0;
  }



  get fullTime() {
    return moment.utc(this.currentDiff + this.duration).format(Config.DURATION_FORMAT);
  }

}
