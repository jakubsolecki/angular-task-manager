import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { TasksService } from '../services/TasksService';
import { Task } from '../models/task.model';

@Component({
    selector: 'tasks-list',
    templateUrl: './tasks-list.component.html',
    styleUrls: ['./tasks-list.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TasksListComponent implements OnInit {

    tasks: Task[] = [];

    constructor(private tasksService: TasksService, private changeDetectionRef: ChangeDetectorRef) { }

    ngOnInit() {
        this.tasks = this.tasksService.getTasks();
        this.tasksService.eventEmitter.subscribe(() => {
            this.tasks = this.tasksService.getTasks();
            this.changeDetectionRef.detectChanges();
        })
    }
}
